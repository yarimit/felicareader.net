﻿using Felica;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Devices.Enumeration;
using Windows.Devices.SmartCards;

namespace FelicaReader.NET {
    class Program {
        private static SmartCardReader reader_ = null;
        private string Uid_ = "";

        static void Main(string[] args) {
            Console.WriteLine($"Run FelicaReader infinite");

            new Program().PollingInfinite().Wait();
        }

        private async Task<bool> InitializeReaderAsync() {
            // Reader検索
            var selector = SmartCardReader.GetDeviceSelector(SmartCardReaderKind.Any);
            var devices = await DeviceInformation.FindAllAsync(selector);

            var device = devices.Where(d => {
                return d.Name.StartsWith("Sony FeliCa Port/PaSoRi");
            }).FirstOrDefault();

            if (device == null) {
                Console.WriteLine("Sony FeliCa Port/PaSoRi not found.");
                return false;
            }

            reader_ = await SmartCardReader.FromIdAsync(device.Id);
            if (reader_ == null) {
                Console.WriteLine("Get SmartCardReader Failed.");
                return false;
            }

            Console.WriteLine("Initialize SmartCardReader Success.");
            return true;

        }

        private async Task PollingInfinite() {
            // SmartCardReader初期化
            if (await InitializeReaderAsync() == false) {
                return;
            }

            // ポーリング
            while (true) {
                await PollingAsync();
                await Task.Delay(100);
            }
        }


        private async Task PollingAsync() {
            // カード検索
            var cards = await reader_.FindAllCardsAsync();
            var card = cards.FirstOrDefault();
            if (card == null) {
                Uid_ = "";
                return;
            }

            try {
                await GetCardInformationAsync(card);
            } catch(Exception ex) {
                Console.Write(ex.StackTrace);
                Console.WriteLine(ex.Message);
            }
        }

        private async Task GetCardInformationAsync(SmartCard card) {
            using (var con = await card.ConnectAsync()) {
                // AccessHandler初期化
                var handler = new AccessHandler(con);

                // UID取得
                var uid = await handler.GetUidAsync();
                var _uid = BitConverter.ToString(uid);
                if(Uid_ == _uid) {
                    // 前回と同カード
                    return;
                }
                Uid_ = _uid;

                //
                // http://iccard.jennychan.at-ninja.jp/format/suica.xml
                //
                // IDm/PMm
                var idmPmm = await ReadIdmPmm(handler, 0x0003);
                var idm = idmPmm.Item1;
                var pmm = idmPmm.Item2;
                Console.WriteLine($"IDm:{BitConverter.ToString(idm)}");
                Console.WriteLine($"PMm:{BitConverter.ToString(pmm)}");

                // 属性情報
                {
                    var data = await ReadWithoutEncryption(handler, idm, 0x008b, 0x01, new byte[] { 0x80, 0x00, });
                    SuicaAttributeBlock attr = new SuicaAttributeBlock(data.BlockData);
                    Console.WriteLine($"属性情報 - {attr.ToString()}");
                }

                // 利用履歴
                for (int b = 0; b < 20; b++) {
                    var history = await ReadWithoutEncryption(handler, idm, 0x090f,
                        1, new byte[] { 0x80, (byte)b });
                    SuicaUseHistoryBlock hist = new SuicaUseHistoryBlock(history.BlockData);
                    Console.WriteLine($"利用履歴({b}) - {hist.ToString()}");
                }

                // 改札入出場履歴
                for (int b = 0; b < 3; b++) {
                    var inout = await ReadWithoutEncryption(handler, idm, 0x108f,
                        1, new byte[] { 0x80, (byte)b });
                    SuicaInOutBlock hist = new SuicaInOutBlock(inout.BlockData);
                    Console.WriteLine($"改札入出場履歴({b}) - {hist.ToString()}");
                }

                // SF入場記録
                for (int b = 0; b < 2; b++) {
                    var data = await ReadWithoutEncryption(handler, idm, 0x10cb,
                        1, new byte[] { 0x80, (byte)b });
                    SuicaSFEnterBlock hist = new SuicaSFEnterBlock(data.BlockData);
                    Console.WriteLine($"SF入場記録({b}) - {hist.ToString()}");
                }

                // 定期券面情報
                {
                    var data = await ReadWithoutEncryption(handler, idm, 0x1888,
                        1, new byte[] { 0x80, 0 });
                    if(data.StatusFlag1 == 0 || data.StatusFlag2 == 0) {
                        SuicaTeikiBlock0 block0 = new SuicaTeikiBlock0(data.BlockData, idm, pmm);
                        Console.WriteLine($"定期券面情報(0) - {block0.ToString()}");
                    } else {
                        Console.WriteLine($"定期券面情報(0) - ERROR:{data.StatusFlag1}-{data.StatusFlag2}");
                    }
                }
            }
        }

        public async Task<byte[]> ReadIdm(
            AccessHandler handler,
            UInt16 systemCode) {
            return (await ReadIdmPmm(handler, systemCode)).Item1;
        }

        public async Task<Tuple<byte[], byte[]>> ReadIdmPmm(
            AccessHandler handler,
            UInt16 systemCode) {
            byte systemCodeHigher = (byte)(systemCode >> 8);
            byte systemCodeLower = (byte)(systemCode & 0x00ff);

            byte[] commandData = new byte[] {
               0x00, 0x00, systemCodeHigher, systemCodeLower, 0x01, 0x0f,
            };
            commandData[0] = (byte)commandData.Length;

            byte[] result = await handler.TransparentExchangeAsync(commandData);

            byte[] idm = new byte[8];
            byte[] pmm = new byte[8];
            Array.Copy(result, 2, idm, 0, idm.Length);
            Array.Copy(result, 10, pmm, 0, pmm.Length);

            return Tuple.Create(idm, pmm);
        }

        public async Task<ReadWithoutEncryptionResponse> ReadWithoutEncryption(
            AccessHandler handler,
            byte[] idm,
            UInt16 serviceCode,
            byte blockNumber,
            byte[] blockList) {
            byte serviceCodeHigher = (byte)(serviceCode >> 8);
            byte serviceCodeLower = (byte)(serviceCode & 0x00ff);

            byte[] commandDataPrefix = new byte[] {
                0x00,                                           // Length
                0x06,                                           // CommandCode(Read Without Encryption)
                0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // IDm
                0x01,                                           // サービス数
                serviceCodeLower, serviceCodeHigher,            // ServiceCode
                blockNumber,                                    // ブロック数
                // block list
            };

            for (int i = 0; i < idm.Length; i++) {
                commandDataPrefix[i + 2] = idm[i];
            }

            byte[] commandData = new byte[commandDataPrefix.Length + blockList.Length];
            Array.Copy(commandDataPrefix, commandData, commandDataPrefix.Length);
            Array.Copy(blockList, 0, commandData, commandDataPrefix.Length, blockList.Length);
            commandData[0] = (byte)commandData.Length;

            byte[] result = await handler.TransparentExchangeAsync(commandData);
            return ReadWithoutEncryptionResponse.ParsePackage(result);
        }
    }
}
