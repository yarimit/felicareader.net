﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace FelicaReader.NET {
    public class ReadWithoutEncryptionResponse {
        public byte[] PacketData { get; set; }

        public byte[] IDm { get; set; }

        public byte StatusFlag1 { get; set; }

        public byte StatusFlag2 { get; set; }

        public byte BlockNumber { get; set; }

        public byte[] BlockData { get; set; }

        public static ReadWithoutEncryptionResponse ParsePackage(
            byte[] packetData) {
            // カード情報の解析
            // len 0x07 IDm(8 byte) status(2 byte) block数 <<data>>
            byte responseLen = packetData[0];

            byte[] idm = new byte[8];
            Array.Copy(packetData, 2, idm, 0, idm.Length);

            byte status1 = packetData[10];
            byte status2 = packetData[11];

            if(packetData.Length < 13) {
                return new ReadWithoutEncryptionResponse() {
                    PacketData = packetData,
                    IDm = idm,
                    StatusFlag1 = status1,
                    StatusFlag2 = status2,
                    BlockNumber = 0,
                    BlockData = new byte[0],
                };
            }

            byte outBlockNumber = packetData[12];
            byte[] data = new byte[outBlockNumber * 16];
            Array.Copy(packetData, 13, data, 0, data.Length);

            return new ReadWithoutEncryptionResponse() {
                PacketData = packetData,
                IDm = idm,
                StatusFlag1 = status1,
                StatusFlag2 = status2,
                BlockNumber = outBlockNumber,
                BlockData = data,
            };
        }
    }
}