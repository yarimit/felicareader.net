﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FelicaReader.NET {
    class BitConvertUtil {
        public static short ToInt16BE(byte[] data, int startIndex) {
            byte[] _data = new byte[] { data[startIndex + 1], data[startIndex] };
            return BitConverter.ToInt16(_data, 0);
        }

        public static Tuple<int, int, int> ParseYMD(byte[] data, int startIndex) {
            byte y = (byte)((data[startIndex] & 0xFE) >> 1);
            byte m = (byte)(((data[startIndex] & 0x01) << 3) | ((data[startIndex + 1] & 0xE0) >> 5));
            byte d = (byte)((data[startIndex + 1] & 0x1F));

            return Tuple.Create((int)y, (int)m, (int)d);
        }

        public static int ParseBCD(byte b) {
            var h = (byte)((b & 0xf0) >> 4);
            var l = (byte)(b & 0x0f);

            return h * 10 + l;
        }

        public static byte[] Scramble(byte[] data, byte[] idm, byte[] pmm, bool even) {
            byte[] _xor = new byte[16];
            if (even) {
                // block 0, 2, 4
                Array.Copy(idm, 0, _xor, 0, 8);
                Array.Copy(pmm, 0, _xor, 8, 8);
            } else {
                // block 1, 3, 5
                Array.Copy(pmm, 0, _xor, 0, 8);
                Array.Copy(idm, 0, _xor, 8, 8);
            }

            byte[] rdata = new byte[16];
            for (int i = 0; i < 16; i++) {
                rdata[i] = (byte)(data[i] ^ _xor[i]);
            }
            return rdata;
        }
    }

    // 機器種別
    class SuicaMachineType {
        private static Dictionary<byte, string> MachineTypeDictionary_ = new Dictionary<byte, String>() {
            { 0x03, "のりこし精算機" },
            { 0x04, "携帯端末" },
            { 0x05, "バス等車載機" },
            { 0x07, "カード発売機" },
            { 0x08, "自動券売機" },
            { 0x09, "SMART ICOCA クイックチャージ機?" },

            { 0x12, "自動券売機" },
            { 0x14, "駅務機器" },
            { 0x15, "定期券 / 特急券発売機" },
            { 0x16, "自動改札機" },
            { 0x17, "簡易改札機" },
            { 0x18, "駅務機器" },
            { 0x19, "窓口処理機" },
            { 0x1A, "窓口処理機" },
            { 0x1B, "モバイルFeliCa" },
            { 0x1C, "入場券券売機" },
            { 0x1D, "他社乗換自動改札機" },
            { 0x1F, "入金機" },

            { 0x20, "発行機?" },
            { 0x21, "入金機" },
            { 0x22, "簡易改札機(ことでん)/PASMOバス/PASPY等窓口処理機" },
            { 0x34, "カード発売機?" },
            { 0x35, "バス等車載機" },
            { 0x36, "バス等車載機" },
            { 0x46, "ビューアルッテ端末" },
            { 0xC7, "物販端末" },
            { 0xC8, "物販端末" },
        };

        public static string Parse(byte b) {
            if (MachineTypeDictionary_.ContainsKey(b)) {
                return MachineTypeDictionary_[b];
            }
            return "???";
        }
    }

    class SuicaUsageType {
        private static Dictionary<byte, string> UsageTypeDictionary = new Dictionary<byte, String>() {
            { 0x01, "自動改札機出場" },
            { 0x02, "SFチャージ" },
            { 0x03, "きっぷ購入" },
            { 0x04, "磁気券精算" },
            { 0x05, "乗越精算" },
            { 0x06, "窓口出場" },
            { 0x07, "新規" },
            { 0x08, "控除" },
            { 0x0D, "バス等" },
            { 0x0F, "バス等" },
            { 0x11, "再発行?" },
            { 0x13, "料金出場" },
            { 0x14, "オートチャージ" },
            { 0x1F, "バス等チャージ" },
            { 0x46, "物販" },
            { 0x48, "ポイントチャージ" },
            { 0x4B, "入場・物販" },
        };

        public static string Parse(byte b) {
            if (UsageTypeDictionary.ContainsKey(b)) {
                return UsageTypeDictionary[b];
            }
            return "???";
        }
    }

    class SuicaInOutType {
        private static Dictionary<byte, string> InOutTypeDictionary = new Dictionary<byte, String>() {
            { 0x01, "入場" },
            { 0x02, "入場 → 出場" },
            { 0x03, "定期入場 → 出場" },
            { 0x04, "入場 → 定期出場" },
            { 0x0E, "窓口出場" },
            { 0x0F, "入場 → 出場" },
            { 0x12, "料金定期入場 → 料金出場" },
            { 0x17, "入場 → 出場" },
            { 0x21, "入場 → 出場" },
        };

        public static string Parse(byte b) {
            if (InOutTypeDictionary.ContainsKey(b)) {
                return InOutTypeDictionary[b];
            }
            return "???";
        }
    }

    class SuicaPaymentType {
        private static Dictionary<byte, string> PaymentTypeDictionary = new Dictionary<byte, String>() {
            { 0x00, "現金/なし" },
            { 0x02, "VIEW" },
            { 0x0B, "PiTaPa" },
            { 0x0D, "オートチャージ対応PASMO" },
            { 0x3F, "モバイルSuica" },
        };
        public static string Parse(byte b) {
            if (PaymentTypeDictionary.ContainsKey(b)) {
                return PaymentTypeDictionary[b];
            }
            return "???";
        }
    }


    class SuicaBlock {
    }

    /// <summary>
    /// 属性情報
    /// </summary>
    class SuicaAttributeBlock : SuicaBlock {
        public int Balance { get; set; }
        public int SeqNo { get; set; }

        public SuicaAttributeBlock(byte[] block) {
            Balance = BitConverter.ToInt16(block, 11);
            SeqNo = BitConvertUtil.ToInt16BE(block, 14);
        }

        public override string ToString() {
            var builder = new StringBuilder();
            builder.AppendFormat("残額:{0},", Balance);
            builder.AppendFormat("取引通番:{0},", SeqNo);
            return builder.ToString();
        }
    }

    /// <summary>
    /// 利用履歴
    /// </summary>
    class SuicaUseHistoryBlock : SuicaBlock {
        /// <summary>
        /// 機器種別
        /// </summary>
        private byte MachineType { get; set; }

        /// <summary>
        /// 利用種別
        /// </summary>
        private byte UsageType { get; set; }

        private byte PaymentType { get; set; }
        private byte InOutType { get; set; }

        private int Year { get; set; }
        private int Month { get; set; }
        private int Day { get; set; }

        private int SeqNo { get; set; }
        private int Balance { get; set; }

        public SuicaUseHistoryBlock(byte[] block) {
            MachineType = block[0];
            UsageType = (byte)(block[1] & 0x7f);
            PaymentType = block[2];
            InOutType = block[3];

            var t = BitConvertUtil.ParseYMD(block, 4);

            Year = t.Item1;
            Month = t.Item2;
            Day = t.Item3;

            SeqNo = BitConvertUtil.ToInt16BE(block, 13);
            Balance = BitConverter.ToInt16(block, 10);
        }

        public override string ToString() {
            var builder = new StringBuilder();
            builder.AppendFormat("機器種別:{0},", SuicaMachineType.Parse(MachineType));
            builder.AppendFormat("利用種別:{0},", SuicaUsageType.Parse(UsageType));
            builder.AppendFormat("支払種別:{0},", SuicaPaymentType.Parse(PaymentType));
            builder.AppendFormat("入出場種別:{0},", SuicaInOutType.Parse(InOutType));
            builder.AppendFormat("処理日付:{0}/{1}/{2},", Year, Month, Day);

            builder.AppendFormat("取引通番:{0},", SeqNo);
            builder.AppendFormat("残額:{0},", Balance);

            return builder.ToString();
        }
    }

    /// <summary>
    /// 改札入出場履歴
    /// </summary>
    class SuicaInOutBlock : SuicaBlock {
        private byte InOutType { get; set; }
        private byte InternalType { get; set; }
        private byte InOutStationCode { get; set; }
        private int MachineNo { get; set; }
        private int Year { get; set; }
        private int Month { get; set; }
        private int Day { get; set; }

        private int Hour { get; set; }
        private int Minute { get; set; }

        private int Price { get; set; }

        public SuicaInOutBlock(byte[] block) {
            InOutType = block[0];
            InternalType = block[1];
            MachineNo = BitConvertUtil.ToInt16BE(block, 4);

            var t = BitConvertUtil.ParseYMD(block, 6);
            Year = t.Item1;
            Month = t.Item2;
            Day = t.Item3;

            Hour = BitConvertUtil.ParseBCD(block[8]);
            Minute = BitConvertUtil.ParseBCD(block[9]);


            Price = BitConverter.ToInt16(block, 10);
        }

        public override string ToString() {
            var builder = new StringBuilder();
            builder.AppendFormat("入出場、使用装置種別:{0},", InOutType);
            builder.AppendFormat("中間改札種別:{0},", InternalType);
            builder.AppendFormat("装置番号:{0},", MachineNo);
            builder.AppendFormat("処理日時:{0}/{1}/{2} {3}:{4},",
                Year, Month, Day, Hour, Minute);
            builder.AppendFormat("金額:{0},", Price);

            return builder.ToString();
        }
    }

    /// <summary>
    /// SF入場記録
    /// </summary>
    class SuicaSFEnterBlock : SuicaBlock {

        private byte Line1 { get; set; }
        private byte Station1 { get; set; }
        private byte Line2 { get; set; }
        private byte Station2 { get; set; }
        private byte Line3 { get; set; }
        private byte Station3 { get; set; }
        private byte Line4 { get; set; }
        private byte Station4 { get; set; }

        private int Price { get; set; }

        public SuicaSFEnterBlock(byte[] block) {
            Line1 = block[0];
            Station1 = block[1];
            Line2 = block[2];
            Station2 = block[3];
            Line3 = block[4];
            Station3 = block[5];
            Line4 = block[6];
            Station4 = block[7];

            Price = BitConvertUtil.ToInt16BE(block, 8);
        }

        public override string ToString() {
            var builder = new StringBuilder();

            if (Line1 != 0) {
                builder.AppendFormat("入場駅コード:{0}/{1},", Line1, Station1);
            }
            if (Line2 != 0) {
                builder.AppendFormat("乗り継ぎ駅コード1:{0}/{1},", Line2, Station2);
            }
            if (Line3 != 0) {
                builder.AppendFormat("乗り継ぎ駅コード2:{0}/{1},", Line3, Station3);
            }
            if (Line4 != 0) {
                builder.AppendFormat("乗り継ぎ駅コード3:{0}/{1},", Line4, Station4);
            }

            builder.AppendFormat("収受金額:{0},", Price);

            return builder.ToString();
        }
    }

    class SuicaTeikiBlock : SuicaBlock {
        protected byte[] Data { get; set; }
        protected byte[] Scrambled { get; set; }

        public SuicaTeikiBlock(
            byte[] data,
            byte[] idm,
            byte[] pmm,
            bool even) {
            Scrambled = data;
            Data = BitConvertUtil.Scramble(data, idm, pmm, even);
        }
    }

    class SuicaTeikiBlock0 : SuicaTeikiBlock {
        // カード有効終了年月日
        private int ExpireYear { get; set; }
        private int ExpireMonth { get; set; }
        private int ExpireDay { get; set; }

        // 原券発行日
        private int OriginYear { get; set; }
        private int OriginMonth { get; set; }
        private int OriginDay { get; set; }

        // 開始年月日
        private int BeginYear { get; set; }
        private int BeginMonth { get; set; }
        private int BeginDay { get; set; }

        // 終了年月日
        private int EndYear { get; set; }
        private int EndMonth { get; set; }
        private int EndDay { get; set; }


        public SuicaTeikiBlock0(byte[] block, byte[] idm, byte[] pmm) : base(block, idm, pmm, true) {
            {
                var t = BitConvertUtil.ParseYMD(Data, 2);
                ExpireYear = t.Item1;
                ExpireMonth = t.Item2;
                ExpireDay = t.Item3;
            }

            {
                var t = BitConvertUtil.ParseYMD(Data, 4);
                OriginYear = t.Item1;
                OriginMonth = t.Item2;
                OriginDay = t.Item3;
            }

            {
                var t = BitConvertUtil.ParseYMD(Data, 6);
                BeginYear = t.Item1;
                BeginMonth = t.Item2;
                BeginDay = t.Item3;
            }

            {
                var t = BitConvertUtil.ParseYMD(Data, 8);
                EndYear = t.Item1;
                EndMonth = t.Item2;
                EndDay = t.Item3;
            }
        }

        public override string ToString() {
            var builder = new StringBuilder();
            builder.AppendFormat("Data:{0},", BitConverter.ToString(Data));
            builder.AppendFormat("Scrambled:{0},", BitConverter.ToString(Scrambled));

            builder.AppendFormat("カード有効終了年月日:{0}/{1}/{2},",
                ExpireYear, ExpireMonth, ExpireDay);
            builder.AppendFormat("原券発行日:{0}/{1}/{2},",
                OriginYear, OriginMonth, OriginDay);
            builder.AppendFormat("開始年月日:{0}/{1}/{2},",
                BeginYear, BeginMonth, BeginDay);
            builder.AppendFormat("終了年月日:{0}/{1}/{2},",
                EndYear, EndMonth, EndDay);
            return builder.ToString();
        }
    }
}